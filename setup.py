from setuptools import setup

setup(name='dagger4',
    version='0.0.dev0',
    description='Dagger4 client libraries',
    url='',
    author='',
    author_email='',
    license='GNU AGPLv3',
    packages=['dagger4'],
    zip_safe=False,
    install_requires=[
        'websocket-client==0.47.0',
        'lru-dict==1.1.6',
    ],
    scripts=[
        'scripts/dagger4_dcallables',
        'scripts/dagger4_docker_container_id',
        'scripts/dagger4_worker',
    ],
)