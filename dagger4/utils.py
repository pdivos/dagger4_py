from datetime import datetime
log_fname = 'log_' + str(int(datetime.utcnow().timestamp())) + '.txt'

def log(*msg):
    import os
    import sys
    if os.environ.get('LOG') == '0':
        return
    ts = datetime.utcnow().isoformat()
    msg = ts + ': ' + ', '.join([m if type(m) is str else repr(m) for m in msg])
    print(msg)
    sys.stdout.flush()
    with open(log_fname, 'a') as f:
        f.write(msg + '\n')

def sha1(v):
    assert type(v) in (bytes, bytearray)
    import hashlib
    return hashlib.sha1(v).digest()

def hex(v):
    import binascii
    return binascii.hexlify(v).decode('utf-8')

class redirect_textio:
    def __init__(self, module, attr, buffer, tee=False):
        import io
        assert isinstance(buffer, io.BytesIO)
        assert isinstance(module.__getattribute__(attr), io.TextIOBase)
        self.module = module
        self.buffer = buffer
        self.tee = tee
        self.attr = attr

    def __enter__(self):
        import io
        self.saved = getattr(self.module, self.attr)
        self.override = io.TextIOWrapper(self.buffer, encoding=self.saved.encoding)
        if self.tee:
            self.override._write = self.override.write
            def twrite(iowrapperself, s):
                iowrapperself._write(s)
                self.saved.write(s)
            self.override.write = twrite.__get__(self.override, io.TextIOWrapper)
        setattr(self.module, self.attr, self.override)
        return self.override

    def __exit__(self, type, val, traceback):
        self.override.flush()
        self.override.detach()
        setattr(self.module, self.attr, self.saved)

import contextlib
@contextlib.contextmanager
def redirect_std(outbuf, errbuf, tee=False):
    """
    # Redirects stdout and stderr into BytesIO buffers.
    import sys
    import io
    myout = io.BytesIO()
    myerr = io.BytesIO()
    with redirect_std(myout, myerr, tee=True):
        print('tee stdout')
        print('tee stderr', file=sys.stderr)

    """
    import sys
    import io
    assert isinstance(outbuf, io.BytesIO)
    assert isinstance(errbuf, io.BytesIO)
    with redirect_textio(sys, 'stdout', outbuf, tee) as stdout_textio:
        with redirect_textio(sys, 'stderr', errbuf, tee) as stderr_textio:
            yield (stdout_textio, stderr_textio)

def run(func, args, raise_on=None, tee=False):
    import io
    import traceback
    value, exception, stacktrace = None, None, None
    outbuf, errbuf = io.BytesIO(), io.BytesIO()
    try:
        with redirect_std(outbuf, errbuf, tee):
            value = func(*args)
        suceeded = True
    except BaseException as e:
        exception = e
        stacktrace = traceback.format_exc()
        suceeded = False
    return (suceeded, value, outbuf.getvalue(), errbuf.getvalue(), exception, stacktrace)

def in_docker():
    import os
    return os.path.isfile('/.dockerenv')

def docker_container_id():
    "returns the id of the docker container that we are currently running inside of"
    assert in_docker()
    # https://stackoverflow.com/questions/20995351/docker-how-to-get-container-information-from-within-the-container
    with open('/proc/self/cgroup', 'r') as myfile:
        data=myfile.read()
    def is_container_id(s):
        return len(s) == len('50636a80b30782d31cbd079057867d3b834aa2a9f1f006115e8abaabd26fc84d') and len(set(s) - set('0123456789abcdef')) == 0
    container_id = None
    for line in data.split('\n'):
        if '/docker/' in line and is_container_id(line.split('/docker/')[-1]):
            _container_id = line.split('/docker/')[-1]
            if container_id is None:
                container_id = _container_id
            else:
                assert container_id == _container_id
    assert container_id is not None
    return container_id

def exec_import_statement(import_statement):
    """
    execs an import statement by safely checking that it is indeed an import statement
    """
    assert import_statement.startswith('import '), "import statement needs to start with 'import'"
    exec(import_statement)

def b64encode(s):
    assert type(s) is str
    import base64
    return base64.b64encode(s.encode('utf-8')).decode('ascii')

def b64decode(s):
    assert type(s) is str
    import base64
    return base64.b64decode(s.encode('ascii')).decode('utf-8')
