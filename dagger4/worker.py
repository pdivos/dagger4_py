from dagger4.client import Client
from dagger4.core import DNodeKey, DNodeKeyRunning, DNodeResult, DRef, DFunType, DException
from dagger4.utils import log

class DFunCache():
    """
    stores actually callable dfun implementations
    and their sources
    """
    def __init__(self):
        self._dfun_cache = {}
    def register_fun(self, dfuntype, name, fun, source):
        assert type(dfuntype) == DFunType
        assert callable(fun)
        assert isinstance(name, str)
        assert isinstance(source, str)
        assert not self._dfun_cache.get(name), "attempted to redefine dfun: " + name
        # dcallable hash is a unique id of the source code
        # this is a preliminary way of computing it because doesn't take into account dependency function calls
        # see https://docs.google.com/spreadsheets/d/1bx6xQOiXle_FqmUygi6Oti_JrPJKoO4W58yj41Xj4Do/edit#gid=1694437618 for details
        from dagger4.utils import sha1, hex
        dcallable_hash = hex(sha1(bytes(source,'utf8')))
        self._dfun_cache[name] = (dfuntype, fun, source, dcallable_hash)
    def get_type(self, name):
        assert name in self._dfun_cache, name
        return self._dfun_cache[name][0]
    def get_fun(self, name):
        assert name in self._dfun_cache, name
        return self._dfun_cache[name][1]
    def get_source(self, name):
        assert name in self._dfun_cache, name
        return self._dfun_cache[name][2]
    def get_dcallable_hash(self, name):
        assert name in self._dfun_cache, name
        return self._dfun_cache[name][3]

class Globals():
    """
    home for all globals held by the single-threaded dagger process
    """

    # "flag whether we are in local exec mode"
    # is_local_exec = False # if set to false, missig deps wont' be registered as missing. use for debugging only

    "dnodekey instance that is being executed right now, so we know what to register missing deps to"
    _dnodekey = None
    @staticmethod
    def set_dnodekey(dnodekey):
        assert type(dnodekey) is DNodeKey or dnodekey is None
        Globals._dnodekey = dnodekey
    @staticmethod
    def get_dnodekey():
        return Globals._dnodekey

    "global client instance"
    _dclient = None
    @staticmethod
    def set_dclient(client):
        assert type(client) is Client or 'DServer' in str(type(client)) or client is None, str(type(client)) # DServer is allowed as a hack for local testing
        Globals._dclient = client
    @staticmethod
    def get_dclient():
        return Globals._dclient

    "function cache storing the registered DFuns of the worker"
    dfun_cache = DFunCache()

    fiddle_stack = [] # stack of fiddles applied by the Fiddle context manager

    _func_dfuntypes = {}
    @staticmethod
    def dfuntype(func):
        try:
            ret = Globals._func_dfuntypes[func]
        except KeyError:
            raise Exception("Attempting to call unknown function: " + func)
        assert type(ret) is DFunType
        return ret

    _child_to_value = {}

    dcommitset = None

class DMissingDep(Exception):
    "this is thrown by DLambda decorator to signal a missing dependency"
    pass

def _ensure(lru_serializer, success, value, stdout, stderr, exception, stacktrace):
    "serilaize results"
    try:
        value = lru_serializer.ensure(value)
        stdout = lru_serializer.ensure(stdout)		
        stderr = lru_serializer.ensure(stderr)	
        stacktrace = stacktrace # don't serialize stacktrace here, will be serialized once wrapped in dexception	
    except BaseException as e:
        # if a serialization error happens then overwrite any earlier exception/stacktrace
        exception = e
        import traceback
        stacktrace = traceback.format_exc()
        success = False
        value = None
    return (success, value, stdout, stderr, exception, stacktrace)

# def _check_unused_children(success, stacktrace):
#     "check after run if all children were used"
#     # checking if there were any unused children and failing node if any
#     # this could happen if in second pass node needs other children than in first pass
#     # which is either a bug in node (for example requesting deps that are not used in the end)
#     # or serialization jitter: same object serializes differently in second pass that triggers
#     # a new child and first will be unused
#     unused_children = set(Globals._child_to_value.keys()) - Globals._child_to_value_used
#     if len(unused_children) != 0:
#         success = False
#         stacktrace = "unused children: " + repr(unused_children)
#     return (success, stacktrace)

def _run(lru_serializer, fun, args, tee = False):
    # seems a bit messy to wrap run in a _run
    # but we need to handle exception occurring during serialization
    # which is common if user is trying to serialize unknown type
    from dagger4.utils import run
    
    success, value, stdout, stderr, exception, stacktrace = run(fun, args, tee = tee)
    success, value, stdout, stderr, exception, stacktrace = _ensure(lru_serializer, success, value, stdout, stderr, exception, stacktrace)
    # success, stacktrace = _check_unused_children(success, stacktrace)
    return (success, value, stdout, stderr, exception, stacktrace)

import enum
class Status(enum.IntEnum):
    SUCCEED = 0
    EXCEPTION = 1
    MISSINGDEP = 2
    
def execute_dnode(client, dnodekeyrunning, tee = False):
    """
      executes a DNodeKeyRunning
      dnodekeyrunning: dnodekeyrunning to execute to
      olddeps: list of existing evaled deps of the node, needed to check if Eval can be called
      cache: used by DFunction to access dep values
      returns a DNode which contains the results of the run
    """
    assert isinstance(client, Client)
    assert type(dnodekeyrunning) is DNodeKeyRunning
    dcall = dnodekeyrunning.dnodekey.dcall
    fun = Globals.dfun_cache.get_fun(dcall.func)
    if fun is None:
        stdout = stderr = None
        stacktrace = client.lru_serializer.ensure('Unable to find function: ' + dcall.func)
        client.complete(dnodekeyrunning, DNodeResult(False, stacktrace, stdout, stderr))
        return Status.EXCEPTION
    assert callable(fun)
    args = [client.lru_serializer.resolve(a) for a in dcall.args]
    assert type(args) is list
    assert len(Globals.fiddle_stack) == 0
    success, value, stdout, stderr, exception, stacktrace = _run(client.lru_serializer, fun, args, tee = tee)
    # assert len(Globals.fiddle_stack) == 0, "not all fiddles were popped, something wrong: " + repr(Globals.fiddle_stack)
    Globals.fiddle_stack = [] # resetting to empty stack in case it didn't reset by itself due to exception

    if success == True:
        client.complete(dnodekeyrunning, DNodeResult(True, value, stdout, stderr))
        return Status.SUCCEED
    elif success == False:
        # DException and DMissingDep exceptions are special to dagger and need to be handled specially
        if type(exception) is DException:
            # client node code has the opportunity to catch and handle dagger exceptions
            # but if it doesn't, we re-throw it in this way and just add the current node in it's list of nodes
            stacktrace = exception.stacktrace
            dnodekeys = [el for el in exception.dnodekeys]
            dnodekeys.append(Globals.get_dnodekey())
            dexception = DException(stacktrace, dnodekeys)
            dexception = client.lru_serializer.ensure(dexception)
            client.complete(dnodekeyrunning, DNodeResult(False, dexception, stdout, stderr))
            return Status.EXCEPTION
        elif type(exception) is DMissingDep:
            # missing dep is raised on a missing dep, we just need to return this
            # server already knows, no need to send any additional request
            return Status.MISSINGDEP
        else:
            # all other exceptions happening during regular business are wrapped in a DExcpetion
            # we don't keep the exception type, just the stack trace as a string because
            # otherwise it would be hard to be cross-platform
            assert exception is not None and type(stacktrace) is str
            dexception = DException(stacktrace, [Globals.get_dnodekey()])
            dexception = client.lru_serializer.ensure(dexception)
            client.complete(dnodekeyrunning, DNodeResult(False, dexception, stdout, stderr))
            return Status.EXCEPTION
    else:
        assert False, success

def worker_loop(server_host, worker_id, worker_repo, stop_on_done = False, 
                # on_failed_dcallback = None,
                sleep_secs = 1, tee = False, graceful_stop_on_SIGINT = True):
    """
    worker_repo: dcommit of the worker, defines the queue the worker will listen on. in theory this could be figured out by the worker itself but it's easier to pass it in from the server
    on_failed_dcallback: DEvent name of the dnodekeyrunning that should be dcalled on a failure
                         callback must accept following args:
                            dnodekeyrunning, stdout, stderr, source, stacktrace
                         if None then no dnodekeyrunning will be called
    """

    from dagger4.client import Client
    client = Client(worker_id, host=server_host)

    running = [True]

    if graceful_stop_on_SIGINT:
        import signal
        original_signal_handler = signal.getsignal(signal.SIGINT)
        def my_signal_handler(sig, frame):
                log('SIGINT received, stopping gracefully')
                running[0] = False
        signal.signal(signal.SIGINT, my_signal_handler)

    Globals.set_dclient(client)
    waiting_for_work_logged = False
    while running[0]:
        resp = client.get_work(worker_repo, 0 if stop_on_done else sleep_secs)
        if resp is None:
            if stop_on_done:
                log('no more work, exiting')
                break
            else:
                if not waiting_for_work_logged:
                    log('waiting for work...')
                waiting_for_work_logged = True
                continue
        waiting_for_work_logged = False
        dnodekeyrunning = resp['dnodekeyrunning']
        Globals._func_dfuntypes = client.lru_serializer.deserialize(resp['func_dfuntypes'])
        Globals._func_dfuntypes = {k:DFunType(v) for k,v in Globals._func_dfuntypes.items()} # construct DFunType from int
        Globals._child_to_value = {}
        _child_to_value = resp['child_to_value']
        # decoding shitty encoding of continuous array, see server code that constructs this
        for i in range(0,len(_child_to_value),2):
            dnodekey = _child_to_value[i]
            dnoderesult = _child_to_value[i+1]
            from dagger4.core import DNodeKey, DNodeResult
            assert type(dnodekey) is DNodeKey and type(dnoderesult) is DNodeResult
            Globals._child_to_value[dnodekey] = dnoderesult
        Globals._child_to_value_used = set()
        assert type(dnodekeyrunning) is DNodeKeyRunning
        Globals.set_dnodekey(dnodekeyrunning.dnodekey)
        Globals.dcommitset = dnodekeyrunning.dcommitset
        log(repr(dnodekeyrunning))
        status = execute_dnode(client, dnodekeyrunning, tee=tee)
        Globals.dcommitset = None
        Globals.set_dnodekey(None)
        Globals._child_to_value = None
        Globals._func_dfuntypes = None
        # if status == Status.EXCEPTION:
            # if on_failed_dcallback is not None and dnodekeyrunning.func != on_failed_dcallback:
            #     from dagger4.utils import now
            #     client.call(DNodeKeyRunning(DFunType.EVENT, on_failed_dcallback, 
            #                      [dnodekeyrunning, value.stdout, value.stderr, value.source, value.stacktrace], now()))
        log('\t=> ' + repr(status))
    Globals.set_dclient(None)

    if graceful_stop_on_SIGINT:
        signal.signal(signal.SIGINT, original_signal_handler)
        log('SIGINT received, stopped gracefully')
