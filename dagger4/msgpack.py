# coding: utf-8

import sys
import struct

assert sys.version_info >= (3, 5), "only works with Python 3.5 or above"
assert not hasattr(sys, 'pypy_version_info'), "not implemented, see https://raw.githubusercontent.com/msgpack/msgpack-python/master/msgpack/fallback.py for full source"

from io import BytesIO

TYPE_IMMEDIATE          = 0
TYPE_ARRAY              = 1
TYPE_MAP                = 2
TYPE_EXT                = 5

DEFAULT_RECURSE_LIMIT = 511

_unpack_from = struct.unpack_from

def _deserialize_header(buffer, buff_i):
    """
    buffer: bytes ocntaining the data
    buff_i: position where this function should start reading from
    returns: tuple of:
        buff_i: new position after some bytes were consumed by the function
        typ: one of TYPE_ARRAY, TYPE_MAP, TYPE_EXT and TYPE_IMMEDIATE
        value: depending on the value of typ:
            TYPE_ARRAY: number of elements in the array that are starting at the new position buff_i
            TYPE_MAP: number of key-value pairs in the array that are starting at the new position buff_i
            TYPE_IMMEDIATE: an object directly encoded in messagepack, such as: None, bool, int, float, str, bytes
            TYPE_EXT: tuple of (type id, length) of extension type
    """
    typ = TYPE_IMMEDIATE
    value = None
    b = buffer[buff_i]
    buff_i += 1
    if b & 0b10000000 == 0: # positive fixint
        value = b
    elif b & 0b11100000 == 0b11100000: # negative fixint
        value = -1 - (b ^ 0xff)
    elif b & 0b11100000 == 0b10100000: # fixstr
        n = b & 0b00011111
        value = buffer[buff_i:buff_i+n]
        buff_i += n
        value = value.decode('utf8')
    elif b & 0b11110000 == 0b10010000: # fixarray
        value = b & 0b00001111
        typ = TYPE_ARRAY
    elif b & 0b11110000 == 0b10000000: # fixmap
        value = b & 0b00001111
        typ = TYPE_MAP
    elif b == 0xc0: # nil
        value = None
    elif b == 0xc2: # false
        value = False
    elif b == 0xc3: # true
        value = True
    elif b == 0xc4:  # bin 8
        n = buffer[buff_i]
        buff_i += 1
        value = buffer[buff_i:buff_i+n]
        buff_i += n
    elif b == 0xc5: # bin 16
        n = _unpack_from(">H", buffer, buff_i)[0]
        buff_i += 2
        value = buffer[buff_i:buff_i+n]
        buff_i += n
    elif b == 0xc6: # bin 32
        n = _unpack_from(">I", buffer, buff_i)[0]
        buff_i += 4
        value = buffer[buff_i:buff_i+n]
        buff_i += n
    elif b == 0xc7:  # ext 8
        typ = TYPE_EXT
        L, n = _unpack_from('Bb', buffer, buff_i)
        buff_i += 2
        value = (n, L)
    elif b == 0xc8:  # ext 16
        typ = TYPE_EXT
        L, n = _unpack_from('>Hb', buffer, buff_i)
        buff_i += 3
        value = (n, L)
    elif b == 0xc9:  # ext 32
        typ = TYPE_EXT
        L, n = _unpack_from('>Ib', buffer, buff_i)
        buff_i += 5
        value = (n, L)
    elif b == 0xca: # float 32
        value = _unpack_from(">f", buffer, buff_i)[0]
        buff_i += 4
    elif b == 0xcb: # float 64
        value = _unpack_from(">d", buffer, buff_i)[0]
        buff_i += 8
    elif b == 0xcc: # uint 8
        value = buffer[buff_i]
        buff_i += 1
    elif b == 0xcd: # uint 16
        value = _unpack_from(">H", buffer, buff_i)[0]
        buff_i += 2
    elif b == 0xce: # uint 32
        value = _unpack_from(">I", buffer, buff_i)[0]
        buff_i += 4
    elif b == 0xcf: # uint 64
        value = _unpack_from(">Q", buffer, buff_i)[0]
        buff_i += 8
    elif b == 0xd0: # int 8
        value = _unpack_from("b", buffer, buff_i)[0]
        buff_i += 1
    elif b == 0xd1: # int 16
        value = _unpack_from(">h", buffer, buff_i)[0]
        buff_i += 2
    elif b == 0xd2: # int 32
        value = _unpack_from(">i", buffer, buff_i)[0]
        buff_i += 4
    elif b == 0xd3: # int 64
        value = _unpack_from(">q", buffer, buff_i)[0]
        buff_i += 8
    elif b == 0xd4:  # fixext 1
        typ = TYPE_EXT
        typeid = _unpack_from("b", buffer, buff_i)[0]
        buff_i += 1
        value = (typeid,1)
    elif b == 0xd5:  # fixext 2
        typ = TYPE_EXT
        typeid = _unpack_from("b", buffer, buff_i)[0]
        buff_i += 1
        value = (typeid,2)
    elif b == 0xd6:  # fixext 4
        typ = TYPE_EXT
        typeid = _unpack_from("b", buffer, buff_i)[0]
        buff_i += 1
        value = (typeid,4)
    elif b == 0xd7:  # fixext 8
        typ = TYPE_EXT
        typeid = _unpack_from("b", buffer, buff_i)[0]
        buff_i += 1
        value = (typeid,8)
    elif b == 0xd8:  # fixext 16
        typ = TYPE_EXT
        typeid = _unpack_from("b", buffer, buff_i)[0]
        buff_i += 1
        value = (typeid,16)
    elif b == 0xd9: # str 8
        n = buffer[buff_i]
        buff_i += 1
        value = buffer[buff_i:buff_i+n]
        buff_i += n
        value = value.decode('utf8')
    elif b == 0xda: # str 16
        n, = _unpack_from(">H", buffer, buff_i)
        buff_i += 2
        value = buffer[buff_i:buff_i+n]
        buff_i += n
        value = value.decode('utf8')
    elif b == 0xdb: # str 32
        n, = _unpack_from(">I", buffer, buff_i)
        buff_i += 4
        value = buffer[buff_i:buff_i+n]
        buff_i += n
        value = value.decode('utf8')
    elif b == 0xdc: # array 16
        typ = TYPE_ARRAY
        value, = _unpack_from(">H", buffer, buff_i)
        buff_i += 2
    elif b == 0xdd: # array 32
        typ = TYPE_ARRAY
        value, = _unpack_from(">I", buffer, buff_i)
        buff_i += 4
    elif b == 0xde: # map 16
        value, = _unpack_from(">H", buffer, buff_i)
        buff_i += 2
        typ = TYPE_MAP
    elif b == 0xdf: # map 32
        value, = _unpack_from(">I", buffer, buff_i)
        buff_i += 4
        typ = TYPE_MAP
    else:
        raise Exception("Unknown header: 0x%x" % b)
    return buff_i, typ, value

def _deserialize(buffer, buff_i):
    global _deserializers
    assert type(buffer) is bytes
    assert type(buff_i) is int and buff_i >= 0 and buff_i < len(buffer)
    buff_i, typ, value = _deserialize_header(buffer, buff_i)

    if typ == TYPE_ARRAY:
        ret = []
        for i in range(value):
            buff_i, r = _deserialize(buffer, buff_i)
            ret.append(r)
        return buff_i, ret
    elif typ == TYPE_MAP:
        ret = {}
        for _ in range(value):
            buff_i, key = _deserialize(buffer, buff_i)
            buff_i, value = _deserialize(buffer, buff_i)
            ret[key] = value
        return buff_i, ret
    elif typ == TYPE_EXT:
        typeid, n = value
        found = False
        for el in _deserializers:
            if el['typeid'][0] == typeid and buffer[buff_i:buff_i+len(el['typeid'])-1] == el['typeid'][1:]:
                found = True
                buff_i += len(el['typeid'])-1
                n -= len(el['typeid'])-1
                value = el['deser'](buffer, buff_i, n, _deserialize)
                buff_i += n
                break
        assert found is True
        return buff_i, value
    else:
        assert typ == TYPE_IMMEDIATE
        return buff_i, value

        
def _serialize_nil(buffer):
    buffer.write(b"\xc0")
    
def _serialize_bool(buffer, obj):
    assert type(obj) is bool
    if obj:
        buffer.write(b"\xc3")
    else:
        buffer.write(b"\xc2")
        
def _serialize_int(buffer, obj):
    assert type(obj) is int
    if 0 <= obj < 0x80:
        buffer.write(struct.pack("B", obj))
    elif -0x20 <= obj < 0:
        buffer.write(struct.pack("b", obj))
    elif 0x80 <= obj <= 0xff:
        buffer.write(struct.pack("BB", 0xcc, obj))
    elif -0x80 <= obj < 0:
        buffer.write(struct.pack(">Bb", 0xd0, obj))
    elif 0xff < obj <= 0xffff:
        buffer.write(struct.pack(">BH", 0xcd, obj))
    elif -0x8000 <= obj < -0x80:
        buffer.write(struct.pack(">Bh", 0xd1, obj))
    elif 0xffff < obj <= 0xffffffff:
        buffer.write(struct.pack(">BI", 0xce, obj))
    elif -0x80000000 <= obj < -0x8000:
        buffer.write(struct.pack(">Bi", 0xd2, obj))
    elif 0xffffffff < obj <= 0xffffffffffffffff:
        buffer.write(struct.pack(">BQ", 0xcf, obj))
    elif -0x8000000000000000 <= obj < -0x80000000:
        buffer.write(struct.pack(">Bq", 0xd3, obj))
    else:
        raise OverflowError("Integer value out of range")
        
def _serialize_float(buffer, obj):
    assert type(obj) is float
    # return buffer.write(struct.pack(">Bf", 0xca, obj)) # single precision float 32
    return buffer.write(struct.pack(">Bd", 0xcb, obj)) # double precision float 64

def _serialize_str(buffer, obj):
    assert type(obj) is str
    obj = obj.encode('utf-8', 'strict')
    n = len(obj)
    if n <= 0x1f:
        buffer.write(struct.pack('B', 0xa0 + n))
    elif n <= 0xff:
        buffer.write(struct.pack('>BB', 0xd9, n))
    elif n <= 0xffff:
        buffer.write(struct.pack(">BH", 0xda, n))
    elif n <= 0xffffffff:
        buffer.write(struct.pack(">BI", 0xdb, n))
    else:
        raise ValueError('Raw is too large')
    buffer.write(obj)

def _serialize_bin_header(buffer, n):
    if n <= 0xff:
        return buffer.write(struct.pack('>BB', 0xc4, n))
    elif n <= 0xffff:
        return buffer.write(struct.pack(">BH", 0xc5, n))
    elif n <= 0xffffffff:
        return buffer.write(struct.pack(">BI", 0xc6, n))
    else:
        raise ValueError('Bin is too large')

def _serialize_array(buffer, obj):
    n = len(obj)
    if n <= 0x0f:
        buffer.write(struct.pack('B', 0x90 + n))
    elif n <= 0xffff:
        buffer.write(struct.pack(">BH", 0xdc, n))
    elif n <= 0xffffffff:
        buffer.write(struct.pack(">BI", 0xdd, n))
    else:
        raise ValueError("Array is too large")
    for i in range(n):
        _serialize(buffer, obj[i])

def _serialize_map(buffer, obj):
    assert type(obj) is dict
    n = len(obj)
    if n <= 0x0f:
        buffer.write(struct.pack('B', 0x80 + n))
    elif n <= 0xffff:
        buffer.write(struct.pack(">BH", 0xde, n))
    elif n <= 0xffffffff:
        buffer.write(struct.pack(">BI", 0xdf, n))
    else:
        raise ValueError("Dict is too large")
    bin_pairs = []
    for (k, v) in obj.items():
        buf = BytesIO()
        _serialize(buf,k)
        bin_k = buf.getvalue()
        buf = BytesIO()
        _serialize(buf,v)
        bin_v = buf.getvalue()
        bin_pairs.append((bin_k,bin_v))
    bin_pairs = sorted(bin_pairs,key=lambda pair:pair[0])
    for (k, v) in bin_pairs:
        buffer.write(k)
        buffer.write(v)

def _serialize_ext(buffer, code, data):
    assert type(code) is int and code >= 0 and code < 128
    L = len(data)
    assert type(L) is int and L > 0
    if L == 1:
        buffer.write(b'\xd4')
    elif L == 2:
        buffer.write(b'\xd5')
    elif L == 4:
        buffer.write(b'\xd6')
    elif L == 8:
        buffer.write(b'\xd7')
    elif L == 16:
        buffer.write(b'\xd8')
    elif L <= 0xff:
        buffer.write(struct.pack(">BB", 0xc7, L))
    elif L <= 0xffff:
        buffer.write(struct.pack(">BH", 0xc8, L))
    elif L <= 0xffffffff:
        buffer.write(struct.pack(">BI", 0xc9, L))
    else:
        raise ValueError('Ext is too large')
    buffer.write(struct.pack("b", code))
    buffer.write(data)

def _serialize(buffer, obj):
    global _serializers
    if obj is None:
        _serialize_nil(buffer)
    elif type(obj) is bool:
        _serialize_bool(buffer,obj)
    elif type(obj) is int:
        _serialize_int(buffer, obj)
    elif type(obj) is float:
        _serialize_float(buffer, obj)
    elif type(obj) in (bytes, bytearray):
        n = len(obj)
        _serialize_bin_header(buffer, n)
        buffer.write(obj)
    elif type(obj) is str:
        _serialize_str(buffer, obj)
    elif type(obj) is memoryview:
        n = len(obj) * obj.itemsize
        _serialize_bin_header(buffer, n)
        buffer.write(obj)
    elif type(obj) is list:
        _serialize_array(buffer, obj)
    elif type(obj) is dict:
        _serialize_map(buffer, obj)
    else:
        for el in _serializers:
            if el['pred'](obj):
                typeid = el['typeid']
                code = int(typeid[0])
                buf = BytesIO()
                buf.write(typeid[1:])
                el['ser'](buf, obj, _serialize)
                data = buf.getvalue()
                buf.close()
                _serialize_ext(buffer, code, data)
                return
        raise TypeError("Cannot serialize %r" % (obj, ))

def serialize(obj):
    """
    obj: object to serialize
    returns: bytes of serialized object
    """
    buffer = BytesIO()
    _serialize(buffer, obj)
    return buffer.getvalue()

def deserialize(buffer):
    """
    buffer: bytes containing the serialized object
    returns: deserialized object
    """
    buff_i = 0
    buff_i, ret = _deserialize(buffer, buff_i)
    assert buff_i == len(buffer)
    return ret

_serializers = []
_deserializers = []

def register_deserializer(typeid, deser):
    """
    typeid is a bytes of length at least 1.
        the first byte in bytes is the messagepack extension type
        the rest are the first bytes of the messagepack extension type payload
        given a byte stream we must be able to uniquely determine which deserializer to use.
        therefore no typeid of any serializer can be such that another deserializer's typeid starts with the given one.
    deser is a callable that
        takes 4 args as input:
            buffer: a bytes
            buff_i: starting position within buffer where the payload starts, right after the typeid
            n: number of payload bytes
            deserializer: a callable that takes buffer, buff_i as input and returns a tuple of (buff_i, deserialized object)
                this is needed so that the deserializer can deserialize child objects
        returns:
            a deserialized object
    """
    assert type(typeid) is bytes and len(typeid) >= 1
    assert callable(deser)
    def hex(v):
        import binascii
        return binascii.hexlify(v).decode('utf-8')
    global _deserializers
    for el in _deserializers:
        assert not el['typeid'].startswith(typeid) and not typeid.startswith(el['typeid']), "ambiguous typeid " + hex(typeid) + ", colliding with " + hex(el['typeid'])
    _deserializers.append({
        'typeid': typeid,
        'deser': deser
    })

def register_serializer(priority, typeid, predicate, serializer):
    """
    priority is an int or float determining the search order among all serializers
    typeid is a bytes of len at least 1 satisfying same conditions as described in register_deserializer
        apart from the important difference that exactly same typeids are allowed, so you are able to serialize into same
        typeid with different serializer methods.
    predicate is a callable taking obj as input and returning True/False depending on
        whether the serializer can be applied or not.
        the first one that can be applied will be applied.
    serializer is a callable
        taking input arguments:
            buffer: writeable buffer where the serializer is expected to .write() the serialized object. doesn't need to write typeid, we take care of that.
            obj: an object to serialize that has predicate(obj)==True
            _serialize: a method that takes buffer and obj as input and writes serialized version of the object onto the stream,
                this is used to serialize child objects. btw the _serialize method from this module will be passed as this arg
        returns: None
    """
    assert type(priority) in (int,float)
    assert type(typeid) is bytes and len(typeid) >= 1
    assert callable(predicate)
    assert callable(serializer)
    global _serializers
    _serializers.append({
        'priority': priority,
        'typeid': typeid,
        'pred': predicate,
        'ser': serializer
    })
    _serializers = sorted(_serializers,key=lambda el:el['priority'])

def _test():    
    obj = {'a':1,'b':True,'c':bytes([1,2,3,4]),'d':[4,'c',False,None]}
    assert deserialize(serialize(obj)) == obj
    obj = {'int': 1, 'float': 0.5, 'boolean': True, 'null': None, 'string': 'foo bar', 'array': ['foo', 'bar'], 'object': {'foo':1, 'baz': 0.5}}
    assert deserialize(serialize(obj)) == obj
    buf = "87 A3 69 6E 74 01 A5 66 6C 6F 61 74 CB 3F E0 00 00 00 00 00 00 A7 62 6F 6F 6C 65 61 6E C3 A4 6E 75 6C 6C C0 A6 73 74 72 69 6E 67 A7 66 6F 6F 20 62 61 72 A5 61 72 72 61 79 92 A3 66 6F 6F A3 62 61 72 A6 6F 62 6A 65 63 74 82 A3 66 6F 6F 01 A3 62 61 7A CB 3F E0 00 00 00 00 00 00"
    buf = bytes.fromhex(buf.replace(" ", ""))
    assert obj == deserialize(buf)
    
    _np_typeid = bytes.fromhex("40ff01")
    def _np_pred(obj):
        import numpy as np
        return type(obj) is np.ndarray
    def _np_serializer(buffer, obj, ser):
        import numpy as np
        assert type(obj) is np.ndarray
        buffer.write(obj.tostring())
    def _np_deserializer(buffer, buff_i, n, deser):
        import numpy as np
        return np.frombuffer(buffer[buff_i:buff_i+n])
    register_serializer(0, _np_typeid, _np_pred, _np_serializer)
    register_deserializer(_np_typeid, _np_deserializer)
    
    _pd_typeid = bytes.fromhex("40ff02")
    def _pd_pred(obj):
        import pandas as pd
        return type(obj) is pd.DataFrame
    def _pd_serializer(buffer, obj, ser):
        import pandas as pd
        assert type(obj) is pd.DataFrame
        obj.to_pickle(buffer, compression=None)
    def _pd_deserializer(buffer, buff_i, n, deser):
        import pandas as pd
        buf = BytesIO(buffer[buff_i:buff_i+n])
        return pd.read_pickle(buf, compression=None)
    register_serializer(0, _pd_typeid, _pd_pred, _pd_serializer)
    register_deserializer(_pd_typeid, _pd_deserializer)
    
    import numpy as np
    import pandas as pd
    obj = {'a': np.array([1.1,2.2,3.3]), 'b': pd.DataFrame([[1.1,2.2],[3.3,4.4]], columns=['a','b'],index=[21,22])}
    assert str(deserialize(serialize(obj))) == str(obj)

if __name__ == '__main__':
    _test()
    from dagger4.register_dagger4_serializers import register
    register()
