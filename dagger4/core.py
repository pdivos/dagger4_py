import enum
import dagger4.utils as utils

FRAME_SIZE = 128 * 1024 # * 1024  # maximum size of any single serialized object
                                # should match the memory capacity of a worker

class DStatus(enum.IntEnum):
    READY = 0           # ready to be evaluated, aka. it is on the work queue
    RUNNING = 1         # being evaluated, aka. has been picked up by a worker
    WAITING = 2         # has deps that are not ready, waiting for those to become available
    COMPLETED = 3       # evaluation successful
    # Note:
    # "executing" means READY, RUNNING or WAITING
    # "completed" means COMPLETEDE

class DFunType(enum.IntEnum):
    LAMBDA = 0     # pure function. can only call another LAMBDA
    FUNCTION = 1   # impure function. can call LAMBDA and FUNCTION
DFunType.fromstring = lambda s: {'LAMBDA':DFunType.LAMBDA,'FUNCTION':DFunType.FUNCTION}[s.upper()]

def is_primitive(v):
    ret = (v is None or type(v) in (bool, float)) or \
          (type(v) == int and v >= -2**63 and v < 2**64)
    return ret

class DRef(bytes):
    "DRef is a key for the key value store which is an SHA1 of a bytes array"
    def __new__(cls, arg):
        assert type(arg) in (str, bytes, bytearray), "DRef must be initialized with str/bytes/bytearray: " + repr(arg)
        if type(arg) == str:
            assert len(arg) == 40, "DRef must be hexadecimal SHA1 value: " + repr(arg)
            return bytes.__new__(cls, bytes.fromhex(arg))
        else:
            assert len(arg) == 20, "DRef should be a 20 byte SHA1 value: " + repr(arg)
            return bytes.__new__(cls, bytes(arg))
    @staticmethod
    def from_buf(buf):
        assert type(buf) in (bytes, bytearray)
        from dagger4.utils import sha1
        return DRef(sha1(buf))
    def __repr__(self):
        return self.__class__.__name__+'('+repr(utils.hex(self))+')'
    def __str__(self):
        return repr(self)
    def __getnewargs__(self):
        return (bytes(self),)

class DCall(tuple):
    """
    DCall is a function call:
        func: string name of function
        args: list of arguments containing primitives or DRef
        ts: timestamp
    """
    __slots__ = []
    def __new__(cls, func, args, ts):
        assert type(func) is str
        if type(args) is list:
            args = tuple(args)
        assert type(args) is tuple
        for arg in args: assert is_primitive(arg) or type(arg) is DRef
        assert type(ts) is int
        return tuple.__new__(cls, (func, args, ts))
    @property
    def func(self):
        return tuple.__getitem__(self, 0)
    @property
    def args(self):
        return tuple.__getitem__(self, 1)
    @property
    def ts(self):
        return tuple.__getitem__(self, 2)
    def __repr__(self):
        reprargs = ','.join(map(repr, self.args))
        if self.ts != 0:
            return self.__class__.__name__+'({}({})@{})'.format(self.func, reprargs, self.ts)
        else:
            return self.__class__.__name__+'({}({}))'.format(self.func, reprargs)
    def __getnewargs__(self):
        return (self.func,self.args,self.ts)
    def drefs(self):
        return [a for a in self.args if type(a) is DRef]
    def to_json(self, dref_to_obj):
        return {
            'func': self.func,
            'args': [dref_to_obj[a] if a in dref_to_obj else a for a in self.args],
            'ts': self.ts
        }

class Fiddle(tuple):
    "one fiddle of a single node"
    __slots__ = []
    def __new__(cls, dcall, value):
        assert type(dcall) is DCall
        assert is_primitive(value) or type(value) is DRef, value
        return tuple.__new__(cls, (dcall, value))
    @property
    def dcall(self):
        return tuple.__getitem__(self, 0)
    @property
    def value(self):
        return tuple.__getitem__(self, 1)
    def __repr__(self):
        return self.__class__.__name__+'({},{})'.format(repr(self.dcall),self.value)
    def __getnewargs__(self):
        return (self.dcall,self.value)
    def drefs(self):
        drefs = self.dcall.drefs()
        if type(self.value) is DRef:
            drefs.append(self.value)
        return list(set(drefs))
    def to_json(self, dref_to_obj):
        return {
            'dcall': self.dcall.to_json(dref_to_obj),
            'value': dref_to_obj[self.value] if self.value in dref_to_obj else self.value
        }

class Fiddles(tuple):
    __slots__ = []
    def __new__(cls, fiddles):
        for f in fiddles: assert type(f) is Fiddle
        assert len(set([f.dcall for f in fiddles])) == len(fiddles), 'dcall not unique'
        # note: sorting happens on serialization, see serializer.py
        return tuple.__new__(cls, tuple(fiddles))
    def add(self, f):
        assert type(f) is Fiddle
        fiddles = [_f for _f in self if _f.dcall != f.dcall]
        fiddles.append(f)
        return Fiddles(fiddles)
    def update(self, fiddles):
        assert type(fiddles) is Fiddles, str(type(fiddles)) + ": " + str(fiddles)
        return Fiddles(list(self) + list(fiddles))
    def contains(self, dcall):
        assert type(dcall) is DCall
        for _dcall, _ in self:
            if _dcall == dcall:
                return True
        return False
    def get(self, dcall):
        assert type(dcall) is DCall
        for _dcall, v in self:
            if _dcall == dcall:
                return v
        assert False
    def __repr__(self):
        s=[]
        for dcall, v in self:
            s.append(repr(dcall)+":"+repr(v))
        return self.__class__.__name__+'[{}]'.format(', '.join(s))
    def __getnewargs__(self):
        return (tuple(self),)
    def drefs(self):
        drefs = []
        for fiddle in self:
            drefs.extend(fiddle.drefs())
        return list(set(drefs))
    def to_json(self, dref_to_obj):
        return [f.to_json(dref_to_obj) for f in self]

class DNodeKey(tuple):
    """
    DNodeKey is the key of a running node
    """
    __slots__ = []
    def __new__(cls, dcall, fiddles):
        assert type(dcall) is DCall
        assert type(fiddles) is Fiddles
        return tuple.__new__(cls, (dcall, fiddles))
    @property
    def dcall(self):
        return tuple.__getitem__(self, 0)
    @property
    def fiddles(self):
        return tuple.__getitem__(self, 1)
    def __repr__(self):
        return 'DNK({},{})'.format(repr(self.dcall), self.fiddles)
    def __getnewargs__(self):
        return (self.dcall,self.fiddles)
    def drefs(self):
        drefs = self.dcall.drefs()
        drefs.extend(self.fiddles.drefs())
        return list(set(drefs))
    def to_json(self, dref_to_obj):
        return {
            'dcall': self.dcall.to_json(dref_to_obj),
            'fiddles': self.fiddles.to_json(dref_to_obj)
        }

class DNodeKeyRunning(tuple):
    """
    DNodeKeyRunning is the key of a running node
    """
    __slots__ = []
    def __new__(cls, dcommitset, dnodekey):
        assert type(dcommitset) is str and '#' in dcommitset, dcommitset
        assert type(dnodekey) is DNodeKey
        return tuple.__new__(cls, (dcommitset, dnodekey))
    @property
    def dcommitset(self):
        return tuple.__getitem__(self, 0)
    @property
    def dnodekey(self):
        return tuple.__getitem__(self, 1)
    def __repr__(self):
        return 'DNKR({},{})'.format(self.dcommitset.split('/')[-1], repr(self.dnodekey))
    def __getnewargs__(self):
        return (self.dcommitset,self.dnodekey)
    def drefs(self):
        drefs = self.dnodekey.drefs()
        return list(set(drefs))
    def to_json(self, dref_to_obj):
        return {
            'dcommitset': self.dcommitset,
            'dnodekey': self.dnodekey.to_json(dref_to_obj),
        }

class DNodeKeyCompleted(tuple):
    """
    DNodeKeyCompleted is the key of a completed node
    """
    __slots__ = []
    def __new__(cls, deep_dcallable_hash, dnodekey):
        assert type(deep_dcallable_hash) is DRef
        assert type(dnodekey) is DNodeKey
        return tuple.__new__(cls, (deep_dcallable_hash, dnodekey))
    @property
    def deep_dcallable_hash(self):
        return tuple.__getitem__(self, 0)
    @property
    def dnodekey(self):
        return tuple.__getitem__(self, 1)
    def __repr__(self):
        return 'DNKC({},{})'.format(repr(self.deep_dcallable_hash), repr(self.dnodekey))
    def __getnewargs__(self):
        return (self.deep_dcallable_hash,self.dnodekey)
    def drefs(self):
        drefs = self.dnodekey.drefs()
        return list(set(drefs))
    def to_json(self, dref_to_obj):
        return {
            'deep_dcallable_hash': self.deep_dcallable_hash,
            'dnodekey': self.dnodekey.to_json(dref_to_obj),
        }

class DNodeResult(tuple):
    """
    DNodeResult is the result of a completed node
    is_success == True: node succeeded, value contains the return value
    is_succedd == False: node failed with an exception, value contains stack trace
    """
    __slots__ = []
    def __new__(cls, is_success, value, stdout, stderr):
        assert type(is_success) is bool
        assert type(value) is DRef or is_primitive(value)
        assert type(stdout) is DRef or is_primitive(stdout)
        assert type(stderr) is DRef or is_primitive(stderr)
        return tuple.__new__(cls, (is_success, value, stdout, stderr))
    @property
    def is_success(self):
        return tuple.__getitem__(self, 0)
    @property
    def value(self):
        return tuple.__getitem__(self, 1)
    @property
    def stdout(self):
        return tuple.__getitem__(self, 2)
    @property
    def stderr(self):
        return tuple.__getitem__(self, 3)
    def __repr__(self):
        return self.__class__.__name__+'({},{},{},{})'.format(self.is_success, repr(self.value), repr(self.stdout), self.stderr)
    def __getnewargs__(self):
        return tuple(self)
    def drefs(self):
        drefs = []
        if type(self.value) is DRef: drefs.append(self.value)
        if type(self.stdout) is DRef: drefs.append(self.stdout)
        if type(self.stderr) is DRef: drefs.append(self.stderr)
        return drefs
    def to_json(self, dref_to_obj):
        return {
            'is_success': self.is_success,
            'value': dref_to_obj[self.value] if self.value in dref_to_obj else self.value,
            'stdout': dref_to_obj[self.stdout] if self.stdout in dref_to_obj else self.stdout,
            'stderr': dref_to_obj[self.stderr] if self.stderr in dref_to_obj else self.stderr
        }

class DServerError(Exception):
    "thrown by server in case of an internal error"
    def __init__(self, message):
        super().__init__(message)

class DException(Exception):
    """
    if a client node fais with exception the stack trace is wrapped into this as a string along with the dnodekey of the node that failed
    this is sent as the result of the node
    when parents invoke a node that resulted with a DException, it will be thrown in the caller, upon calling the node that errored out
    """
    def __init__(self, stacktrace, dnodekeys):
        assert type(stacktrace) is str
        assert type(dnodekeys) is list
        for e in dnodekeys: assert type(e) is DNodeKey
        super().__init__(stacktrace, dnodekeys)
    @property
    def stacktrace(self):
        return self.args[0]
    @property
    def dnodekeys(self):
        return self.args[1]
    def __repr__(self):
        return self.__class__.__name__+'\n{}\nDagger Stacktrace:\n{}'.format(self.stacktrace, '\n'.join([repr(e) for e in self.dnodekeys]))
    def __str__(self):
        return repr(self)

class DRequest(tuple):
    __slots__ = []
    def __new__(cls, method, args, msg_id, __drefstore_c2s__):
        assert type(method) is str
        assert type(args) is list
        assert type(msg_id) is int
        assert type(__drefstore_c2s__) is dict and type(__drefstore_c2s__['drefstore_changes']) is list
        return tuple.__new__(cls, (method, args, msg_id, __drefstore_c2s__))
    @property
    def method(self):
        return tuple.__getitem__(self, 0)
    @property
    def args(self):
        return tuple.__getitem__(self, 1)
    @property
    def msg_id(self):
        return tuple.__getitem__(self, 2)
    @property
    def __drefstore_c2s__(self):
        return tuple.__getitem__(self, 3)

class DResponse(tuple):
    __slots__ = []
    def __new__(cls, response, msg_id, __drefstore_s2c__):
        assert type(msg_id) is int
        assert type(__drefstore_s2c__) is dict and set(__drefstore_s2c__.keys()) == {'worker_id','new_keyvalues','required_keys','total_size'}
        return tuple.__new__(cls, (response, msg_id, __drefstore_s2c__))
    @property
    def response(self):
        return tuple.__getitem__(self, 0)
    @property
    def msg_id(self):
        return tuple.__getitem__(self, 1)
    @property
    def __drefstore_s2c__(self):
        return tuple.__getitem__(self, 2)
