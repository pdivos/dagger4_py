from io import BytesIO

EXT_TYPE_CORE = 0x40 # msgpack ext type of 0x40 denotes a dagger4 core object
EXT_TYPE_CORE_SET = ord('S')
EXT_TYPE_CORE_TUPLE = ord('T')
EXT_TYPE_CORE_DSTATUS = ord('s')
EXT_TYPE_CORE_DFUNTYPE = ord('F')
EXT_TYPE_CORE_DREF = ord('R')
EXT_TYPE_CORE_DCALL = ord('c')
EXT_TYPE_CORE_FIDDLE = ord('d')
EXT_TYPE_CORE_FIDDLES = ord('D')
EXT_TYPE_CORE_DNODEKEY = ord('j')
EXT_TYPE_CORE_DNODEKEYRUNNING = ord('k')
EXT_TYPE_CORE_DNODEKEYCOMPLETED = ord('K')
EXT_TYPE_CORE_DNODERESULT = ord('r')
EXT_TYPE_CORE_DERROR = ord('E')
EXT_TYPE_CORE_DEXCEPTION = ord('e')
EXT_TYPE_CORE_DREQUEST = ord('Q')
EXT_TYPE_CORE_DRESPONSE = ord('P')

def _register_serializers():
    from dagger4.msgpack import register_serializer
    from dagger4.core import DRef, DStatus, DFunType, DCall, Fiddle, Fiddles, DNodeKey, DNodeKeyRunning, DNodeKeyCompleted, DServerError, DRequest, DResponse, DNodeResult, DException
    def _set_ser(buffer, obj, ser):
        assert type(obj) is set
        arr = []
        for el in obj:
            buf = BytesIO()
            ser(buf, el)
            buf = buf.getvalue()
            arr.append(buf)
        arr = sorted(arr)
        ser(buffer, arr)
    register_serializer(
        0,
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_SET]),
        lambda obj: type(obj) is set,
        _set_ser
    )
    register_serializer(
        0,
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_TUPLE]),
        lambda obj: type(obj) is tuple,
        lambda buffer, obj, ser: ser(buffer, list(obj))
    )
    register_serializer(
        0,
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DSTATUS]),
        lambda obj: type(obj) is DStatus,
        lambda buffer, obj, ser: buffer.write(int(obj).to_bytes(1, 'big'))
    )
    register_serializer(
        0,
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DFUNTYPE]),
        lambda obj: type(obj) is DFunType,
        lambda buffer, obj, ser: buffer.write(int(obj).to_bytes(1, 'big'))
    )
    register_serializer(
        0,
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DREF]),
        lambda obj: type(obj) is DRef,
        lambda buffer, obj, ser: buffer.write(bytes(obj))
    )
    register_serializer(
        0,
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DCALL]),
        lambda obj: type(obj) is DCall,
        lambda buffer, obj, ser: ser(buffer,list(obj))
    )
    register_serializer(
        0,
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_FIDDLE]),
        lambda obj: type(obj) is Fiddle,
        lambda buffer, obj, ser: ser(buffer,list(obj))
    )
    register_serializer(
        0,
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_FIDDLES]),
        lambda obj: type(obj) is Fiddles,
        lambda buffer, obj, ser: ser(buffer,sorted(list(obj)))
    )
    register_serializer(
        0,
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DNODEKEY]),
        lambda obj: type(obj) is DNodeKey,
        lambda buffer, obj, ser: ser(buffer,list(obj))
    )
    register_serializer(
        0,
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DNODEKEYRUNNING]),
        lambda obj: type(obj) is DNodeKeyRunning,
        lambda buffer, obj, ser: ser(buffer,list(obj))
    )
    register_serializer(
        0,
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DNODEKEYCOMPLETED]),
        lambda obj: type(obj) is DNodeKeyCompleted,
        lambda buffer, obj, ser: ser(buffer,list(obj))
    )
    register_serializer(
        0,
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DNODERESULT]),
        lambda obj: type(obj) is DNodeResult,
        lambda buffer, obj, ser: ser(buffer,list(obj))
    )
    register_serializer(
        0,
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DREQUEST]),
        lambda obj: type(obj) is DRequest,
        lambda buffer, obj, ser: ser(buffer,list(obj))
    )
    register_serializer(
        0,
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DRESPONSE]),
        lambda obj: type(obj) is DResponse,
        lambda buffer, obj, ser: ser(buffer,list(obj))
    )
    register_serializer(
        0,
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DERROR]),
        lambda obj: type(obj) is DServerError,
        lambda buffer, obj, ser: ser(buffer,obj.args[0])
    )
    register_serializer(
        0,
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DEXCEPTION]),
        lambda obj: type(obj) is DException,
        lambda buffer, obj, ser: ser(buffer,[obj.stacktrace,obj.dnodekeys])
    )

def _register_deserializers():
    from dagger4.msgpack import register_deserializer
    from dagger4.core import DRef, DStatus, DFunType, DCall, Fiddle, Fiddles, DNodeKey, DNodeKeyRunning, DNodeKeyCompleted, DServerError, DRequest, DResponse, DNodeResult, DException

    def _set_deser(buffer, i, n, deser):
        arr = deser(buffer,i)[1]
        _set = set()
        for el in arr:
            assert type(el) is bytes, type(el)
            i, obj = deser(el,0)
            assert i == len(el)
            _set.add(obj)
        return _set

    register_deserializer(
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_SET]),
        _set_deser
    )
    register_deserializer(
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_TUPLE]),
        lambda buffer, i, n, deser: tuple(deser(buffer,i)[1])
    )
    register_deserializer(
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DSTATUS]),
        lambda buffer, i, n, deser: DStatus(int.from_bytes(buffer[i:i+n], 'big'))
    )
    register_deserializer(
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DFUNTYPE]),
        lambda buffer, i, n, deser: DFunType(int.from_bytes(buffer[i:i+n], 'big'))
    )
    register_deserializer(
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DREF]),
        lambda buffer, i, n, deser: DRef(buffer[i:i+n])
    )
    register_deserializer(
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DCALL]),
        lambda buffer, i, n, deser: DCall(*deser(buffer,i)[1])
    )
    register_deserializer(
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_FIDDLE]),
        lambda buffer, i, n, deser: Fiddle(*deser(buffer,i)[1])
    )
    register_deserializer(
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_FIDDLES]),
        lambda buffer, i, n, deser: Fiddles(deser(buffer,i)[1])
    )
    register_deserializer(
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DNODEKEY]),
        lambda buffer, i, n, deser: DNodeKey(*deser(buffer,i)[1])
    )
    register_deserializer(
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DNODEKEYRUNNING]),
        lambda buffer, i, n, deser: DNodeKeyRunning(*deser(buffer,i)[1])
    )
    register_deserializer(
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DNODEKEYCOMPLETED]),
        lambda buffer, i, n, deser: DNodeKeyCompleted(*deser(buffer,i)[1])
    )
    register_deserializer(
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DNODERESULT]),
        lambda buffer, i, n, deser: DNodeResult(*deser(buffer,i)[1])
    )
    register_deserializer(
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DREQUEST]),
        lambda buffer, i, n, deser: DRequest(*deser(buffer,i)[1])
    )
    register_deserializer(
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DRESPONSE]),
        lambda buffer, i, n, deser: DResponse(*deser(buffer,i)[1])
    )
    register_deserializer(
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DERROR]),
        lambda buffer, i, n, deser: DServerError(deser(buffer,i)[1])
    )
    register_deserializer(
        bytes([EXT_TYPE_CORE, EXT_TYPE_CORE_DEXCEPTION]),
        lambda buffer, i, n, deser: DException(*deser(buffer,i)[1])
    )

def _test():
    from dagger4.core import DRef, DStatus, DFunType, DCall, Fiddle, Fiddles, DNodeKey, DNodeKeyRunning, DNodeKeyCompleted, DServerError, DRequest, DResponse, DNodeResult
    obj = {
        'set': set([1,2,3]),
        'tuple': tuple([1,2,3]),
        'dstatus': DStatus.COMPLETED,
        'dfuntype': DFunType.LAMBDA,
        'dref': DRef('0001020304050607080910111213141516171819'),
        'dcall': DCall('func', [], 0),
        'fiddle': Fiddle(DCall('func', [], 0), True),
        'fiddles': Fiddles([Fiddle(DCall('func', [], 0), True)]),
        'dnodekey': DNodeKey(DCall('func', [], 0), Fiddles([Fiddle(DCall('func', [], 0), True)])),
    }
    from dagger4.msgpack import serialize, deserialize
    so = serialize(obj)
    assert deserialize(so) == obj
    
_registered = [False]
def register():
    if _registered[0] is False:
        _registered[0] = True
        _register_serializers()
        _register_deserializers()
        _test()
