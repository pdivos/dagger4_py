# dagger4 package top level objects are the unes used by users in dcallables

from dagger4.register_dagger4_serializers import register
register()

from dagger4.core import DException

def log(*msg):
    # from dagger4.utils import log
    # log(*msg)
    pass

def _callee(func, args, ts):
    "constructs callee dnodekey"
    from dagger4.core import DCall, DNodeKey
    assert type(func) is str, str(type(func)) + "," + repr(func)
    assert type(ts) is int or ts is None, str(type(ts)) + "," + repr(ts)
    from dagger4.worker import Globals
    caller = Globals.get_dnodekey()
    assert type(caller) is DNodeKey
    assert type(args) in (list, tuple), str(type(args)) + "," + repr(args)
    args = [Globals.get_dclient().lru_serializer.ensure(a) for a in args]
    dcall = caller.dcall
    ts = ts or dcall.ts # inherit timestamp from caller if not explicitly provided. even for LAMBDAS, zeroed out on server side
    from dagger4.core import DFunType
    dfuntype = Globals.dfuntype(func)
    if dfuntype == DFunType.LAMBDA:
        ts = 0
    else:
        assert Globals.dfuntype(caller.dcall.func) == DFunType.FUNCTION, "FUNCTION can only be called from FUNCTION"
    fiddles = caller.fiddles  # get fiddles inherited from caller
    # apply fiddles added by Fiddle context manager
    for f in Globals.fiddle_stack:
        fiddles = fiddles.add(f)
    callee = DNodeKey(DCall(func, args, ts), fiddles)
    log('_callee', func, args, caller, callee)
    return callee

def call(func, *args, **kwargs):
    assert set(kwargs.keys()) in (set(),{'ts'}), kwargs
    ts = kwargs.get('ts')
    from dagger4.worker import Globals
    caller = Globals.get_dnodekey()
    dcommitset = Globals.dcommitset
    assert type(dcommitset) is str and '#' in dcommitset, dcommitset
    callee = _callee(func, args, ts)
    client = Globals.get_dclient()
    if caller.fiddles.contains(callee.dcall):
        value = caller.fiddles.get(callee.dcall)
        value = client.lru_serializer.resolve(value)
        log('call', 'fiddle', func, args, caller, value)
        return value
    if callee in Globals._child_to_value:
        Globals._child_to_value_used.add(callee)
        dnoderesult = Globals._child_to_value[callee]
        is_success = dnoderesult.is_success
        value = dnoderesult.value
        value = client.lru_serializer.resolve(value)
        log('call', 'child_to_value', func, args, caller, is_success, value)
        if is_success:
            # succeeded node return values are returned as normal
            return value
        else:
            # failed node return values contain stack trace, these are raised here to emulate exception
            assert type(value) is DException
            log('call', 'raise', value)
            raise value
    from dagger4.core import DNodeKeyRunning
    ret = client.call(DNodeKeyRunning(dcommitset,callee), caller=DNodeKeyRunning(dcommitset,caller))
    from dagger4.core import DStatus
    if ret['dstatus'] == DStatus.COMPLETED:
        is_success = ret['result'].is_success
        value = ret['result'].value
        value = client.lru_serializer.resolve(value)
        log('call', 'completed', is_success, func, args, caller, is_success, value)
        if is_success:
            # succeeded node return values are returned as normal
            return value
        else:
            # failed node return values contain stack trace, these are raised here to emulate exception
            assert type(value) is DException
            log('call', 'raise', value)
            raise value
    else:
        log('call', 'missing', func, args, caller)
        from dagger4.worker import DMissingDep
        raise DMissingDep('missing dep: ' + repr(callee))

class DepCollector(object):
    """
    DepCollector requests deps explicitly without waiting for the function to hit them.
    Use for requiring missing deps are in a loop.
    usage:
    DepCollector().require(func1, args1).require(func2, args2).request()
    """
    def __init__(self):
        self.deps = []
    def require(self, func, *args, **kwargs):
        assert type(func) is str
        assert type(args) in (tuple,list)
        assert set(kwargs.keys()) in (set(),{'ts'}), kwargs
        ts = kwargs.get('ts')
        from dagger4.worker import Globals
        callee = _callee(func, args, ts)
        if not Globals.get_dnodekey().fiddles.contains(callee.dcall) and callee not in Globals._child_to_value:
            self.deps.append(callee)
        return self
    def request(self):
        from dagger4.worker import Globals
        client = Globals.get_dclient()
        i = 0
        batch_size = 1000
        while True:
            deps = self.deps[(i*batch_size):((i+1)*batch_size)]
            if len(deps)==0:
                break
            from dagger4.core import DNodeKeyRunning
            dcommitset = Globals.dcommitset
            caller = DNodeKeyRunning(dcommitset,Globals.get_dnodekey())
            callees = [DNodeKeyRunning(dcommitset,d) for d in deps]
            if not client.call_multiple(callees, caller=caller):
                from dagger4.worker import DMissingDep
                raise DMissingDep()
            i += 1

def iter_deps(func, args, i_arg_to_map, list_of_inputs):
    """
    an efficient generation of deps and iteration through the results
    this is preferred to DepCollector when possible because it's a cleaner interface but under the hood it's the same thing

    runs DepCollector func(args) with subsituting to the i_arg_to_map.th arg the values of list_of_inputs
    returns a list fo same size as list_of_inputs, containing the corresponding results
    iter_deps('myfunc', ['a0',None,'a2'], 1, a1s)

    i_arg_to_map can also be a list of inds in which case list_of_inputs is expected to be a list of tuples, usually created wit zip:
    iter_deps('myfunc', ['a0',None,'a2','None], [1,3], zip(a1s, a3s))
    """
    if type(i_arg_to_map) is int:
        assert i_arg_to_map>=0 and i_arg_to_map<len(args)
        assert args[i_arg_to_map] is None
    elif type(i_arg_to_map) is list:
        for i in i_arg_to_map:
            assert i >= 0 and i < len(args)
            assert args[i] is None
    else:
        assert False, "incorrect type: " + str(i_arg_to_map)
    assert type(list_of_inputs) is list
    dc = DepCollector()
    for inp in list_of_inputs:
        if type(i_arg_to_map) is int:
            args[i_arg_to_map] = inp
        else:
            assert len(inp) == len(i_arg_to_map), str(inp) + ", " + str(i_arg_to_map)
            for i in range(len(i_arg_to_map)):
                args[i_arg_to_map[i]] = inp[i]
        dc.require(func, args)
    dc.request()
    for inp in list_of_inputs:
        if type(i_arg_to_map) is int:
            args[i_arg_to_map] = inp
        else:
            assert len(inp) == len(i_arg_to_map), str(inp) + ", " + str(i_arg_to_map)
            for i in range(len(i_arg_to_map)):
                args[i_arg_to_map[i]] = inp[i]
        yield (inp, call(func, args))

def ts():
    """
    returns ts of currently executing dnodekeyrunning
    """
    from dagger4.worker import Globals
    return Globals.get_dnodekey().dcall.ts

def _dfun(fun, funtype):
    from dagger4.core import DFunType
    assert type(funtype) is DFunType
    assert callable(fun)
    func = fun.__name__
    import inspect
    from dagger4.worker import Globals
    Globals.dfun_cache.register_fun(funtype, func, fun, ''.join(inspect.getsourcelines(fun)[0]))
    def fun_new(*args):
        return call(func, args)
    return fun_new

def DLambda(fun):
    "decorator for marking DLambda"
    from dagger4.core import DFunType
    return _dfun(fun, DFunType.LAMBDA)

def DFunction(fun):
    "decorator for marking DFunction"
    from dagger4.core import DFunType
    return _dfun(fun, DFunType.FUNCTION)

class Fiddle():
    """
    context manager for applying a Fiddle
    """
    def __init__(self, func, args, value, ts = None):
        from dagger4.worker import Globals
        ts = ts or Globals.get_dnodekey().dcall.ts
        from dagger4.core import DFunType
        if Globals.dfuntype(func) == DFunType.LAMBDA:
            ts = 0
        from dagger4.core import DCall
        from dagger4.core import Fiddle as core_Fiddle
        self.fiddle = core_Fiddle(DCall(func, args, ts), value)

    def __enter__(self):
        from dagger4.worker import Globals
        Globals.fiddle_stack.append(self.fiddle)

    def __exit__(self, *args):
        from dagger4.worker import Globals
        _ = Globals.fiddle_stack.pop()
        assert _ == self.fiddle

