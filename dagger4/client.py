import websocket
import contextlib
import functools
import os
import socket

def log(*msg):
    from dagger4.utils import log
    log(*msg)
    # pass

def _current_function_name():
    import inspect
    return inspect.stack()[1][3]

class Client():
    def __init__(self, worker_id, host='127.0.0.1', port=8000):
        self.url = "ws://"+host+":"+str(port)+"/dagger"
        self.worker_id = worker_id
        self.msg_id_autoinc = 0
        from dagger4.lru_serializer import LruSerializer
        from dagger4.core import FRAME_SIZE
        self.lru_serializer = LruSerializer(FRAME_SIZE)

    def _wscall(self, method, *args):
        from dagger4.msgpack import serialize, deserialize
        from dagger4.core import DRequest, DResponse, DServerError
        noresponse_methods = ['complete'] # server sends no reponse for these
        with contextlib.closing(websocket.create_connection(self.url)) as ws:
            drefstore_changes = self.lru_serializer.dvaluestore.pop_changes()
            __drefstore_c2s__ = {
                'worker_id': self.worker_id,
                'drefstore_changes': drefstore_changes
            }
            req = DRequest(method, list(args), self.msg_id_autoinc, __drefstore_c2s__)
            log('client send', method, *args)
            ws.send_binary(serialize(req))
            self.msg_id_autoinc += 1
            if method not in noresponse_methods:
                recv = ws.recv()
                assert type(recv) in [bytes, bytearray], len(recv)
                resp = deserialize(recv)
                assert type(resp) is DResponse
                msg_id = resp.msg_id
                assert msg_id == self.msg_id_autoinc - 1
                response = resp.response
                log('client recv', response)
                __drefstore_s2c__ = resp.__drefstore_s2c__
                assert __drefstore_s2c__['worker_id'] == self.worker_id, str(self.worker_id) + " vs. " + str(__drefstore_s2c__['worker_id']) + " method: " + str(method) + " args: " + str(args) + " response: " + str(response)
                self.lru_serializer.dvaluestore.update(__drefstore_s2c__['new_keyvalues'], __drefstore_s2c__['required_keys'])
                if type(response) is DServerError:
                    raise response
                else:
                    return response

    def get_work(self, dcommit, timeout=None):
        return self._wscall(_current_function_name(), dcommit, timeout)

    def call(self, callee, caller = None):
        return self._wscall(_current_function_name(), callee, caller)

    def call_multiple(self, callees, caller = None):
        return self._wscall(_current_function_name(), callees, caller)

    def complete(self, dnodekeyrunning, dnoderesult):
        from dagger4.core import DNodeKeyRunning, DNodeResult
        assert type(dnodekeyrunning) is DNodeKeyRunning
        assert type(dnoderesult) is DNodeResult
        return self._wscall(_current_function_name(), dnodekeyrunning, dnoderesult)

    def node(self, dnode_id):
        assert type(dnode_id) is int
        return self._wscall(_current_function_name(), dnode_id)

    def source(self, dcallable_hash):
        return self._wscall(_current_function_name(), dcallable_hash)
