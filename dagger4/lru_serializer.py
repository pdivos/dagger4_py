"""
LruSerializer is a serializer with an LRU cache
It contains 3 caches:
 - dref -> dvalue, where dref is the sha256 hash of the serialized bytes array and dvalue is the bytes array. this is in LruSerializer.dvaluestore
 - dref -> obj, where obj is the object instance, this is in LruSerializer.objstore.dref_to_obj
 - obj -> dref, but only for objects that can be used as keys, this is in LruSerializer.objstore.obj_to_dref
"""

from dagger4.core import DRef

def hashable(obj):
    if hasattr(obj, '__hash__') and callable(obj.__hash__) and hasattr(obj, '__eq__') and callable(obj.__eq__):
        try:
            hash(obj)
            return True
        except TypeError:
            return False
    else:
        return False

class ObjStore:
    """
    in-memory cache for deserialized objects
    self.dref_to_obj: DRef -> Object LRU cache
    self.obj_to_dref: Object -> DRef dict for the subset of objs that are hashable
    """
    def __init__(self):
        self.dref_to_obj = {}
        self.obj_to_dref = {}
    def set(self, dref, obj):
        assert type(dref) is DRef
        self.dref_to_obj[dref] = obj
        if hashable(obj):
            self.obj_to_dref[obj] = dref
    def get(self, dref):
        return self.dref_to_obj[dref]
    def contains(self, dref):
        return dref in self.dref_to_obj
    def remove(self, dref):
        assert type(dref) is DRef
        obj = self.dref_to_obj[dref]
        if self.contains_obj(obj):
            del self.obj_to_dref[obj]
        del self.dref_to_obj[dref]
    def contains_obj(self, obj):
        return hashable(obj) and obj in self.obj_to_dref
    def get_dref(self, obj):
        assert hashable(obj)
        return self.obj_to_dref[obj]

def _test_objstore():
    o = ObjStore()
    dref_0 = DRef(b'00000000000000000000')
    obj_0 = {'a':1,'b':2}
    dref_1 = DRef(b'11111111111111111111')
    obj_1 = (1,2)
    o.set(dref_1, obj_1)
    assert o.contains(dref_1)
    assert o.get(dref_1) == obj_1
    assert o.contains_obj(obj_1)
    assert o.get_dref(obj_1) == dref_1
    o.set(dref_0, obj_0)
    assert o.contains(dref_0)
    assert o.get(dref_0) == obj_0
    assert not o.contains_obj(obj_0)

from collections import OrderedDict                                       
class LRUCache(OrderedDict):
    def __init__(self, max_size, on_evicted):
        super().__init__()
        assert type(max_size) is int and callable(on_evicted)             
        self.max_size = max_size          
        self.on_evicted = on_evicted   
                                       
    def _purge(self):
        lengths = []                   
        for buf in super().values():                                          
            lengths.append(len(buf))
        n_pop = 0
        for i in range(len(lengths)-1):
            lengths[len(lengths)-2-i] = lengths[len(lengths)-2-i] + lengths[len(lengths)-1-i]
            if lengths[len(lengths)-2-i] >= self.max_size:
                n_pop += 1
        for i in range(n_pop):
            (k,v) = self.popitem(last=False)
            self.on_evicted(k,v)

    def __getitem__(self, key):
        v = super().__getitem__(key)
        super().__setitem__(key, v)
        self.move_to_end(key)
        return v

    def __setitem__(self, key, buf):
        assert type(buf) is bytes, "can only use tuple of (obj, buf) as value"
        size = len(buf)
        assert size < self.max_size, "item too large: " + str(size) + " vs. " + str(self.max_size)
        super().__setitem__(key, buf)
        self._purge()

def _test_LRUCache():
    dct = LRUCache(16, lambda k,v: print('evicted',k,v))
    from dagger4.core import DRef
    from dagger4.msgpack import serialize
    obj = {'Hello':'world'}
    buf = serialize(obj)
    key = DRef.from_buf(buf)
    dct[key] = buf
    print(dct)
    obj = {'Hello2':'world2'}
    buf = serialize(obj)
    key = DRef.from_buf(buf)
    dct[key] = buf
    print(dct)

class DRefStoreLRU:
    """
    LRU dict that reports it's own changes
    on the server we have a global key->value store
    a subset of this we want to keep on clients in DRefStoreLRU instances which are LRU dicts
    on server for each client we have a DRefSet that maintains the keys that the client knows about
    new key/value pairs can arise both on client and server and can also travel between client/server

    if a new key/value arises on client which is not in DRefStoreLRU then it is added to DRefStoreLRU
        and we note that it needs to be sent to server with the next call. when sent to server it will be added to the global key/value store
        and the key will be noted on the DRefSet elonging to the client
    if a key is dropped on client because it's out of LRU capacity then the key is noted and sent to server so it can update it's DRefSet
    if server needs to respond to a client call then it needs to figure out what key/value from global store are needed by client
        that it doesn't already have and those need to be sent to along client with the response. client needs to make sure when inserting
        these in it's own DRefStoreLRU that these are not mistaken as new key/values that arose from client side and therefore not sent back to server

    example use cases:

    get_work:   server sends all DRefs and bytes of completed child dcalls that the client will need
                when executing the work so it won't have to issue additional call() requests to get values of completed nodes
                but server will only send those drefs that the client doesn't already have

    complete:   client send completed node value and stdouts and failed stacktraces not only as DRefs
                    but in the same go as bytes as well. this again saves tcp packets
    """
    def __init__(self, lru_size, on_evicted = None):
        self.changes = [] # list of evicted keys
        def evicted(dref, buf):
            self.changes.append(['d',dref])
            if on_evicted is not None:
                on_evicted(dref, buf)
        self.dref_to_buf = LRUCache(lru_size, evicted)

    def __repr__(self):
        s = '{\n'
        for k, v in self.dref_to_buf.items():
            s += '\t' + repr(k) + ': ' + repr(v) + '\n'
        s += '}\n'
        return s

    def __str__(self):
        return repr(self)

    def get(self, dref):
        buf = self.dref_to_buf[dref]
        return buf

    def set(self, key, value):
        """
        called by client
        important:
            stuff added here will appear in new_keyvalues
            keys removed due to stuff added here will apper in dropped_keys
        """
        assert type(key) is DRef
        assert type(value) is bytes
        if key not in self.dref_to_buf:
            self.dref_to_buf[key] = value
            self.changes.append(['n',key,value])
        else:
            assert self.dref_to_buf[key] == value, str(self.dref_to_buf[key]) + " vs. " + str(value)

    def contains(self, dref):
        return dref in self.dref_to_buf

    def update(self, new_keyvalues, required_keys):
        """
        update called on the new set of keys received from server
        important:
            anything added through update won't appear in pop_new_keyvalues
            keys removed due to stuff added here will apper in dropped_keys

        new_keyvalues: dict of new kv pairs to be updated
        required_keys: list of existing keys that should not be removed during update
        """
        assert type(new_keyvalues) is dict
        assert type(required_keys) is list

        # check the total size of required keys that we want to keep and new_keyvalues
        # that we want to insert doesn't breach the capacity of the cachje
        total_size = 0
        for k in required_keys:
            assert type(k) is DRef
            assert k in self.dref_to_buf, self.dref_to_buf
            # important! this also triggers k to be brought at the front of LRU cache
            # so thois is not only needed for total_size computatiopn
            total_size += len(self.dref_to_buf[k])
        for k, v in new_keyvalues.items():
            assert type(k) is DRef
            assert type(v) is bytes
            self.dref_to_buf[k] = v
            total_size += len(v)
        assert total_size < self.dref_to_buf.max_size, "trying to store too much in LRU cache: " + str(total_size) + " vs. " + str(self.dref_to_buf.max_size)

    def pop_changes(self):
        """
        returns list of changes where each change is another list
            if change[0] == 'd', then the change is a key drop and change[1] is the dropped dref
            if change[0] == 'n', then the change is a new key added, change[1] is the new DRef and change[2] is the new bytes added
        """
        changes = self.changes
        self.changes = []
        return changes

class LruSerializer:
    """
    de/serializer + lru cache + reports cache changes to server
    """
    def __init__(self, lru_size):
        self.objstore = ObjStore()
        def on_evicted(dref, dvalue):
            assert type(dref) is DRef
            self.objstore.remove(dref)
        self.dvaluestore = DRefStoreLRU(lru_size, on_evicted = on_evicted)

    def serialize(self, obj):
        if self.objstore.contains_obj(obj):
            dref = self.objstore.get_dref(obj)
            # print(115, dref)
        else:
            from dagger4.msgpack import serialize
            buf = serialize(obj)
            dref = DRef.from_buf(buf)
            self.dvaluestore.set(dref, buf)
            self.objstore.set(dref, obj)
            # print(134, dref)
        return dref

    def deserialize(self, dref):
        assert type(dref) is DRef
        if self.objstore.contains(dref):
            # print(139, dref, self.objstore.get(dref))
            return self.objstore.get(dref)
        elif self.dvaluestore.contains(dref):
            from dagger4.msgpack import deserialize
            dvalue = self.dvaluestore.get(dref)
            obj = deserialize(dvalue)
            self.objstore.set(dref, obj)
            # print(142, dref, dvalue, msgpackable, obj)
            return obj
        else:
            assert False, "cannot find dref: " + repr(dref)

    def ensure(self, obj):
        """
        ensure:
            primitive -> primitive
            DRef      -> DRef
            object    -> DRef
        """
        from dagger4.core import is_primitive
        if is_primitive(obj) or type(obj) is DRef:
            return obj
        else:
            return self.serialize(obj)

    def resolve(self, dref):
        """
        resolve:
            primitive -> primitive
            DRef      -> object
            object    -> Exception
        """
        from dagger4.core import is_primitive
        if type(dref) is DRef:
            return self.deserialize(dref)
        else:
            assert is_primitive(dref), 'unknown type: ' + str(type(dref))
            return dref

def _test_lru_serializer():
    ts = LruSerializer(16)
    obj = (1,{2:(3,4)})
    dref = ts.serialize(obj)
    print('objstore.dref_to_obj')
    print(ts.objstore.dref_to_obj)
    print('objstore.obj_to_dref')
    print(ts.objstore.obj_to_dref)
    print('dvaluestore:')
    print(ts.dvaluestore)
    print(ts.deserialize(dref))
    print(dref == ts.serialize(obj))
    obj = (5,{6:(7,8)})
    dref = ts.serialize(obj)
    print('objstore.dref_to_obj')
    print(ts.objstore.dref_to_obj)
    print('objstore.obj_to_dref')
    print(ts.objstore.obj_to_dref)
    print('dvaluestore:')
    print(ts.dvaluestore)
    print(ts.deserialize(dref))
    print(dref == ts.serialize(obj))
    print(ts.dvaluestore.pop_changes())

if __name__ == "__main__":
    _test_objstore()
    _test_lru_serializer()
    _test_LRUCache()
    print('lru_serializer: all tests passed')